%uses the fingerprint db as layer of defense

testTruths = vec2ind(testLabels) - 1;
advTruths = vec2ind(advLabels) - 1;

predTest = net(testData);
predTest = vec2ind(predTest) - 1;
predAdv = net(advData);
predAdv = vec2ind(predAdv) - 1;

tmpImages = vl_imsmooth(testImages, sqrt((binSize/magnif)^2 - 0.25));
tmpImages = single(tmpImages);

threshDefense = 45;
predIdx = zeros(1,size(predTest,2));

fprintf(flog, 'Starting defense evaluation with threshold set to %i \n', threshDefense);

%impact on original testing data
count = 1; tic;
for sampleIdx = 1:1:size(predTest, 2)
   [f,d] = vl_dsift(tmpImages(:,:,sampleIdx), 'size', binSize, 'step', stepSize);
   for fpIdx = 1:1:size(fingerprints{testTruths(sampleIdx)+1, 1}, 3)
       m = vl_ubcmatch(d, fingerprints{testTruths(sampleIdx)+1, 1}(:,:,fpIdx));
       if threshDefense < size(m,2)
           break;
       elseif fpIdx == size(fingerprints{testTruths(sampleIdx)+1, 1}, 3)
           predIdx(1, count) = sampleIdx;
           count = count + 1;
       end
   end
end
predIdx = predIdx(1,1:count-1);

%get accuracy info
tmpData = testData;
tmpData(:, predIdx) = [];
pred = net(tmpData);
pred = vec2ind(pred)-1;
tmpTestLabels = testLabels;
tmpTestLabels(:, predIdx) = [];
truth = vec2ind(tmpTestLabels)-1;
n = nnz(truth - pred);

elapsed_t = toc;

fprintf(flog, 'Samples thrown out: %i/%i \n', size(predIdx,2), size(testData, 2));
fprintf(flog, 'Accuracy on test data using fingerprinting: %3.2f Time: %.2f \n', 100 - (n/size(pred,2))*100, elapsed_t);

%impact on original testing data
tmpImages = vl_imsmooth(advImages, sqrt((binSize/magnif)^2 - 0.25));
tmpImages = single(tmpImages);

count = 1; tic;
for sampleIdx = 1:1:size(predAdv, 2)
   [f,d] = vl_dsift(tmpImages(:,:,sampleIdx), 'size', binSize, 'step', stepSize);
   for fpIdx = 1:1:size(fingerprints{advTruths(sampleIdx)+1, 1}, 3)
       m = vl_ubcmatch(d, fingerprints{advTruths(sampleIdx)+1, 1}(:,:,fpIdx));
       if threshDefense < size(m,2)
           break;
       elseif fpIdx == size(fingerprints{advTruths(sampleIdx)+1, 1}, 3)
           predIdx(1, count) = sampleIdx;
           count = count + 1;
       end
   end
end
predIdx = predIdx(1,1:count-1);

%get accuracy info
tmpData = advData;
tmpData(:, predIdx) = [];
pred = net(tmpData);
pred = vec2ind(pred)-1;
tmpAdvLabels = advLabels;
tmpAdvLabels(:, predIdx) = [];
truth = vec2ind(tmpAdvLabels)-1;
n = nnz(truth - pred);
elapsed_t = toc;

fprintf(flog, 'Samples thrown out: %i/%i \n', size(predIdx,2), size(advData, 2));
fprintf(flog, 'Accuracy on adversarial data using fingerprinting: %3.2f Time: %.2f \n', 100 - (n/size(pred,2))*100, elapsed_t);

%cleanup
clear elapsed_t n truth tmpAdvLabels pred tmpData tempTestLabels predIdx m f d sampleIdx fpIdx tmpImages tmpTestLabels advTruths advPred testPred testTruths