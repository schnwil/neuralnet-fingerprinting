%train neural net using results from gs

fprintf(flog, 'Training network normal samples \n');

net = patternnet([100, 100, 100]);
net.trainFcn = 'trainscg';
net.layers{1}.transferFcn = 'tansig';
net.layers{2}.transferFcn = 'tansig';
net.layers{3}.transferFcn = 'radbas';
net.trainParam.max_fail = 50;
net.trainParam.showWindow = false;
net.input.processFcns = {'mapminmax'}; tic;
net = train(net, trainData, trainLabels, 'useGPU', 'yes'); elapsed_t = toc;

%print results to command window
pred = net(testData);
perf = perform(net, testLabels, pred);
truths = vec2ind(testLabels) - 1;
class = vec2ind(pred) - 1;
testPred = class;
error = (nnz(truths - class)/size(testLabels, 2)*100);
testAcc = 100 - error;
fprintf(flog, 'Training completed in %.3f \n', elapsed_t);
fprintf(flog, 'MNIST Test Data:\n Perf:%3.2f Acc:%3.2f \n', (1-perf)*100, testAcc);

%test adversarial samples and print to window
pred = net(advData);
perf = perform(net, advLabels, pred);
truths = vec2ind(advLabels) - 1;
class = vec2ind(pred) - 1;
advPred = class;
error = (nnz(truths - class)/size(advLabels, 2)*100);
advAcc = 100 - error;
fprintf(flog, 'Adversarial Test:\n Perf:%3.2f Acc:%3.2f \n', (1-perf)*100, advAcc);

%cleanup
clear pred perf elapsed_t truths class error