%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Journal Article
% LaTeX Template
% Version 1.4 (15/5/16)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Frits Wenneker (http://www.howtotex.com) with extensive modifications by
% Vel (vel@LaTeXTemplates.com)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass{article}

\usepackage{blindtext} % Package to generate dummy text throughout this template 

\usepackage[sc]{mathpazo} % Use the Palatino font
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\linespread{1.05} % Line spacing - Palatino needs more space between lines
\usepackage{microtype} % Slightly tweak font spacing for aesthetics

\usepackage[english]{babel} % Language hyphenation and typographical rules

\usepackage[hmarginratio=1:1,top=32mm,columnsep=20pt]{geometry} % Document margins
\usepackage[hang, small,labelfont=bf,up,textfont=it,up]{caption} % Custom captions under/above floats in tables or figures
\usepackage{booktabs} % Horizontal rules in tables

\usepackage{lettrine} % The lettrine is the first enlarged letter at the beginning of the text

\usepackage{enumitem} % Customized lists
\setlist[itemize]{noitemsep} % Make itemize lists more compact

\usepackage{abstract} % Allows abstract customization
\renewcommand{\abstractnamefont}{\normalfont\bfseries} % Set the "Abstract" text to bold
\renewcommand{\abstracttextfont}{\normalfont\small\itshape} % Set the abstract itself to small italic text

\usepackage{titlesec} % Allows customization of titles
\renewcommand\thesection{\Roman{section}} % Roman numerals for the sections
\renewcommand\thesubsection{\roman{subsection}} % roman numerals for subsections
\titleformat{\section}[block]{\large\scshape\centering}{\thesection.}{1em}{} % Change the look of the section titles
\titleformat{\subsection}[block]{\large}{\thesubsection.}{1em}{} % Change the look of the section titles

\usepackage{fancyhdr} % Headers and footers
\pagestyle{fancy} % All pages have headers and footers
\fancyhead{} % Blank out the default header
\fancyfoot{} % Blank out the default footer
\fancyfoot[C]{\thepage} % Custom footer text

\usepackage{titling} % Customizing the title section

\usepackage{hyperref} % For hyperlinks in the PDF

\usepackage{graphicx}
\captionsetup{format=plain}
\usepackage{dblfloatfix}
\usepackage[rightcaption]{sidecap}
\usepackage{multicol, lipsum}
\usepackage{float}

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\setlength{\droptitle}{-4\baselineskip} % Move the title up

\pretitle{\begin{center}\Huge\bfseries} % Article title formatting
\posttitle{\end{center}} % Article title closing formatting
\title{Fingerprinting with SIFT Descriptors to Catch Adversarial Inputs on Neural Networks} % Article title
\author{%
\textsc{William Schneble} \\[1ex] % Your name
\normalsize University of Washington Bothell \\ % Your institution
\normalsize \href{mailto:schnwil@uw.edu}{schnwil@uw.edu} % Your email address
%\and % Uncomment if 2 authors are required, duplicate these 4 lines if more
%\textsc{Jane Smith}\thanks{Corresponding author} \\[1ex] % Second author's name
%\normalsize University of Utah \\ % Second author's institution
%\normalsize \href{mailto:jane@smith.com}{jane@smith.com} % Second author's email address
}
\date{\today} % Leave empty to omit a date
\renewcommand{\maketitlehookd}{%
}
\raggedbottom
%----------------------------------------------------------------------------------------

\begin{document}

% Print the title
\maketitle

%----------------------------------------------------------------------------------------
%	ARTICLE CONTENTS
%----------------------------------------------------------------------------------------

\begin{multicols}{2}

\section*{Abstract}

Machine learning models are often built under the assumption that data inputs can be trusted but are then deployed to environments where adversaries exist. These adversaries can then target and influence the model compromising integrity, availability and privacy. This paper examines exploratory, integrity attacks against neural networks and proposes a defensive measure using SIFT descriptors to fingerprint training data. The accuracy and time required for training and testing data are considered in the evaluation of defenses. The results are also compared against generating and training on adversarial data via Jacobian based saliency mapping. 

%------------------------------------------------

\section{Project Description}

\subsection{Background}

Machine learning is being increasingly used for complicated problems where no formal solutions exist. While attention has been given to classification accuracy, these models generally work under the assumption that the data has not been compromised. However attackers have shown an increasing ability to undermine these models and intrusion detection systems \cite{Corona2013}. Solutions to this problem are usually expensive and a drain on performance such as the Reject on Negative Impact (RONI) model that trains two models for comparisons \cite{Nelson2008}, generating some of the adversarial attacks for the training data \cite{Goodfellow2015}, or using ensemble methods. For large data sets these solutions are often impracticable due to their poor scalability. These solutions also don't change the attacker's perspective of the system as a black box. These issues define the challenges in creating robust machine learning models against adversarial environments.

The scope of applications using machine learning continues to grow from image processing, speech recognition, self-driving cars, medical diagnostics, cybersecurity and many more domains. While machine learning has continued to become more accurate in its classification accuracy they also continue to be extremely perceptible to adversarial attacks \cite{Tygar2011}. For example, it has been shown that an adversary can train a deep neural network (DNN) and learn information about the target network such as feature importance and sensitivity. Thus the attacker is able to craft adversary samples even if the target network never disclosed class probabilities, training, activation functions or other model information. The gravitas of the situation is most apparent in the misclassification of images where the perturbations are un-noticeable to the human eye yet the model makes a seemingly random misclassification \cite{Szegedy2013}. However given the attacker's goal to mis-classify data, this misclassification is definitely not random or harmless. For example, given that machine learning is being used in traffic control and autonomous vehicles, a stop sign being misclassified or ignored could result in serious injury and legal reliability for the manufacturer.

\begin{figure}[H]
	\includegraphics[width=\linewidth]{../Images/adversary_samples2.png}
	\caption{Example of MNIST dataset adversarial sample crafting with the original image on the left and adversarial sample on the right. Labels above the image show original class and targeted class with adversarial perturbation.}
	\label{fig:adversary_samples}
\end{figure}

\subsection{Related Work}

Generative models that create adversary samples for inclusion in the training data of models has already been created and are freely available on GitHub such as Cleverhans \cite{papernot2016cleverhans}. These projects are ideal for generating adversary samples for this project and include recognizable datasets such as the MNIST and CIFAR10 datasets which are frequently used for benchmarking \cite{lecun-mnisthandwrittendigit-2010,cifar10}. Figure \ref{fig:adversary_samples} shows an example of an adversary sample crafted using Cleverhans to \textit{fool} a classifier into labeling a seven a nine.

Previous research has been done on subverting biometric systems such as facial recognition \cite{Biggio2015}. The model used performed feature extraction, a matcher function against a template database and a fusion rule for updating templates in a database. The research focused on generating adversarial templates in the database and was not directly concerned about the model's accuracy and timing requirements. The model also used Principle Component Analysis (PCA) as the means of feature extraction during a preprocessing stage. However, PCA had already been previously demonstrated as weak to adversarial attacks \cite{Tygar2011} while SIFT descriptors has, to the author's knowledge, not been explored in-depth.

SIFT descriptors have been extensively used in making panoramic images from several individual images and landmark detection to cluster similar images \cite{vlfeat}. A SIFT feature is a set of descriptors that are a histogram representation of the image gradients. In addition, the descriptors are invariant to scale, rotation and translation which makes them useful in finding similarities between two images. Similarly, a lack of matches between two sets of descriptors indicates a poor correlation of two sets. This makes SIFT descriptors an interesting prospect for detecting and rejecting adversarial or abnormal inputs.

\section{Design}

\subsection{Overview}

\begin{figure*}
	\centering
	\includegraphics[width=\textwidth]{../Images/overview.png}
	\caption{General outline of project}
	\label{fig:outline}
\end{figure*}

The outline of the project is shown in Figure \ref{fig:outline}. A feed forward neural network is trained and tested with the MNIST dataset. The MNIST training data is also used to create a SIFT descriptor database. New descriptors are added to the database if an incoming training sample fails to meet a threshold number of matches. Adversarial samples are generated by the MNIST dataset and Cleverhans. The neural network then attempts to classify the adversarial data and the results are checked against the descriptor database. For example, if the neural network predicts that sample \textit{i} is a 7 then the sample's SIFT descriptors are checked against the fingerprint database for other 7's. If a certain threshold for the number of matched descriptors is met then the classification is accepted or rejected otherwise.

\textbf{Attacker Capability:} The neural net does not train on incoming test data and thus is not susceptible to causative attacks. Thus the attacker is interested in exploratory, integrity attacks that attempt to mis-classify samples such as 7 to 9. The attacker has access to the entire MNIST dataset and can control any fraction of the test data. The attacker does not however know any specifics about the neural network type, parameters, or preprocessing techniques used but can generate adversarial samples locally on a neural network of their choice \cite{Szegedy2013}.

\begin{figure}[H]
	\includegraphics[width=\linewidth]{../Images/cfm_compare.jpg}
	\caption{Comparison of neural network's confusion matrix for label 3 with the MNIST test data (top) and adversarial crafted samples (bottom).}
	\label{fig:cfm}
\end{figure}

\textbf{Generating Adversarial Samples:} The adversarial samples are created in Python using Cleverhans and a different neural network than the one used for final classification. To increase the odds of misclassification, a confusion matrix of the test data from the neural network is used to determine label to label weaknesses by looking for false negatives and false positives \ref{fig:cfm}. The adversarial samples can then be crafted from the MNIST test data (or from the training data for training the neural network on adversarial samples) using the Jacobian based saliency mapping method.

\textbf{Defense with SIFT:} In addition to training the neural network with the MNIST training data, descriptors are generated for each sample. Descriptors for the sample are then matched for each set of descriptors in the database for the target label and, if a certain threshold is not met, then the descriptors are added to the database. Testing with the database works by first classifying the test input with the neural network and then checking the descriptor database for matches with the test input's descriptors. If the number of matches reaches beyond a threshold then the predicted classification of the test input is accepted or if the threshold is not met then the sample is rejected and thrown out. A comparison of the MNIST test data on the neural network with and without the defense can be used to determine any overhead of said defense. 

\subsection{Metrics}

The following are used to determine overall performance of the neural network and defense:

\begin{itemize}
	\item Accuracy Score: How many samples were correct out of the total sample population. If a sample is thrown out by the descriptor defense and is an adversarial sample then it is treated as a true positive. If the sample thrown was from the normal MNIST test data then it is counted as a false positive.
	\begin{equation} \label{eq:accuracy}
	Accuracy = \frac{\Sigma TP + \Sigma TN}{Total Population}
	\end{equation}
	\item Time to train the network.
	\item Time to generate adversarial samples.
	\item Time to make final prediction.
\end{itemize}

\end{multicols}

\begin{figure}[!ht]
\begin{center}
	\includegraphics[width=\textwidth]{../Images/cfm_defense.jpg}
	\caption{Confusion matrix for the fingerprinting defense}
	\label{fig:cfm_defense}
\end{center}
\end{figure}

\pagebreak

\begin{multicols*}{2}

\section{Results}

The results of the neural network with 10,000 normal and 10,000 adversarial samples is shown in Figure \ref{fig:neural_net_comparison}. The overhead of the fingerprinting defense with normal samples is only 1.3\% in classification accuracy which is also in-line with generating and training on 20,000 adversarial samples (~0.5\% less accurate). Fingerprinting with SIFT descriptors achieved an overall accuracy of 74.1\% whereas the neural network without any defensive measures had its accuracy reduced from 96.7\% to 51.57\%. While the descriptor defense was able to bring the accuracy of the network up significantly, recovering more than half of the accuracy lost by the introduction of adversarial samples, the defense fell short of the accuracy that training on the adversarial was able to achieved (90.8\%). The confusion matrix for the fingerprinting defense with 5,000 normal and 5,000 adversarial samples is shown in Figure \ref{fig:cfm_defense}. The neural network and defense have the most trouble with 4's being misclassified as 9's and 8's as 0's and 3's. These labels are under-represented in the descriptor database such as 9 having almost half the number of descriptors of other labels \ref{fig:fingerprint_dim}. 

While possible to increase the number of descriptors needed to make a match to catch more of the adversarial samples, this also increases the number of false positives disproportionately. Currently the fingerprinting defense catches about half of the adversarial samples with a ratio of 27:1 adversarial samples thrown to normal samples thrown. Increasing percentage of adversarial samples caught to 70\% results in an 8:1 ratio of adversarial to normal samples thrown and increasing to 90\% results in a ratio of 2.5:1. These diminishing returns makes the defense ineffective for small fractions of the input data being controlled by the adversary.

\vfill
\null

\begin{figure}[H]
	\includegraphics[width=\linewidth]{../Images/neural_net_comparison.jpg}
	\caption{Comparison of neural network accuracy with 10,000 normal test samples and 10,000 adversarial samples.}
	\label{fig:neural_net_comparison}
\end{figure}

\begin{figure}[H]
	\includegraphics[width=\linewidth]{../Images/fingerprint_dim.png}
	\caption{Variable fingerprints in MATLAB with labels in ascending order (from top to bottom); third dimension represents the number of descriptors.}
	\label{fig:fingerprint_dim}
\end{figure}

Timing results are summarized in Table \ref{tab:time_results}. Generating the adversarial samples takes the most time by a significant margin and training with generated, adversarial samples also increases neural network training time by roughly ~33\%. However, after the network has been trained on the adversarial samples the predications are near instant for 10,000 samples (0.23 seconds in totality). The fingerprinting defense on the other hand needs to check the descriptor database every time for every test sample resulting in roughly 20 milliseconds per normal sample and 80 milliseconds per adversarial sample. Thus for 10,000 normal samples a total, experimental time of 184 seconds was required and for 10,000 adversarial samples 768 seconds to apply the fingerprinting defense.

\begin{table}[H]
	\includegraphics[width=\linewidth]{../Images/timing_table.jpg}
	\caption{Table of timing results}
	\label{tab:time_results}
\end{table}

\section{Conclusion}

The practicability of using SIFT descriptors as a defense for neural networks is severely weakened by the amount of time required to make predictions and its accuracy compared to other defenses. While using SIFT descriptors takes less time in the short run, the descriptor database has to be checked every new and incoming sample against the fingerprint database which takes significant time and thus the defense is not recommended for long term or high volume deployment. The accuracy compared to a neural network that trained on adversarial samples is also disappointing with a difference of -16.7\% on 10,000 adversarial samples. 

Samples that continued to get through both models in high volume were highlighted in Figure \ref{fig:cfm_defense}. To address these hard to classify labels, a different threshold for descriptor matching would be needed per label. Also, the fingerprint database does not have a uniform number of descriptors per label \ref{fig:fingerprint_dim} with label 1 having twice as many descriptors as other labels such as 0 and 9. Therefore the threshold for adding descriptors to the database also likely needs to by more dynamic. However, an increase in the number of descriptors is not proportional to accuracy as reflect by 0 having half as many descriptors as 1 but relatively the same accuracy. This reinforces the observation that the threshold for the number of descriptors needs to be more dynamic to prevent superfluous new sets from being added while also ensuring samples are not under-represented.

The descriptor defense also introduces a timing difference between normal samples and adversarial samples \ref{tab:time_results}. This deference in timing can be used by the attacker to know how many descriptors and thus how much confidence the model has in the prediction given how long it took for the model to make a final classification. For example, if you have an adversarial sample and send it through the model and get a time of about 80 milliseconds then you know the model went through as many descriptors as a typical adversarial sample, even if your sample is accepted and fools the model. This can lead the attacker to continue crafting their adversarial sample to refine it until less time is required and thus fewer descriptors were checked and the sample more closely resembles a normal sample. Similar timing attacks have frequently been deployed against cryptographic algorithms \cite{LeBlanc2010} and why most crypto libraries have their own digest equals method that returns in constant time. Thus any speedup achieved on normal samples is irrelevant as every sample should return with the same time to avoid this security flaw.

Overall the attempts to increase accuracy are again dwarfed by the feasibility of the defense given the timing requirements. Use of SIFT descriptors for short term models may be of some benefit as it does provide modest gains in performance in an adversarial setting but for medium and long term deployments other defensive techniques yield better results in both time consumption and accuracy. 

\subsection{Future Work}

Scalability is an area of concern due to the increasing number of pixels in images. The MNIST dataset is composed of 28x28 (784) pixels per image whereas a modern cell phone camera is around 10 mega-pixels (10,000,000 pixels per image). How to scale the current descriptor defense with other feature reduction techniques such as PCA is an open question. The current implementation uses a single threaded library for the generation and comparison of SIFT descriptors and could easily be parallelized in future updates. Other parallelization strategies could include MPI and dividing the fingerprint database among several nodes or machines. However the caveat would be why not use these resources to generate adversarial samples concurrently for training purposes since the neural network performed with greater accuracy than its descriptor counterpart \ref{fig:neural_net_comparison}.

Increasing the accuracy of the fingerprinting defense with SIFT descriptors could be improved via implementing a dynamic threshold for adding new samples to the database. Other efforts to increase classification accuracy may also include further parameter tuning of the defense to find the most efficient setup for finding adversarial samples. This requires mostly a grid search approach and is very expensive to run.

Convolution Neural Networks (CNN) also perform feature extraction from input data and may be of better use than SIFT descriptors. However CNNs have been used to good effect as generative models to create adversarial samples \cite{Radford2016}. While ensemble techniques have been shown to still be susceptible to adversarial attacks \cite{Goodfellow2015}, a combination of the previous techniques may yield interesting or fruitful results.

%----------------------------------------------------------------------------------------
%	REFERENCE LIST
%----------------------------------------------------------------------------------------

\nocite{Wang2016, PerdisciRobertoandLeeWenkeandFeamster2010}
\bibliographystyle{plain}
\bibliography{report}

%----------------------------------------------------------------------------------------

\end{multicols*}
\end{document}