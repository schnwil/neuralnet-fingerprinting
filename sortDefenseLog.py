'''
Created on 5/26/17
Uses the defense_gs.txt log to create a CSV file

@author: William Schneble
'''
import pandas as pd

path = './logs'

class instance():
    def __init__(self, magnif, threshFingerprint, threshDefense, fingerprintTime, testSamplesThrown,
                  advSamplesThrown, testAcc, advAcc, testTime, advTime):
        self.magnif = magnif
        self.threshFingerprint = threshFingerprint
        self.threshDefense = threshDefense
        self.fingerprintTime = fingerprintTime
        self.testSamplesThrown = testSamplesThrown
        self.advSamplesThrown = advSamplesThrown
        self.testAcc = testAcc
        self.advAcc = advAcc
        self.testTime = testTime
        self.advTime = advTime
        
    def __str__(self):
        return self.magnif + ' ' + self.threshFingerprint + ' ' + self.threshDefense + ' ' + self.fingerprintTime + ' ' + self.testSamplesThrown + ' ' + self.advSamplesThrown + ' ' + self.testAcc + ' ' + self.advAcc + ' ' + self.testTime + ' ' + self.advTime
    
    def to_dict(self):
        return {
        'magnif': self.magnif,
        'threshFingerprint': self.threshFingerprint,
        'threshDefense': self.threshDefense,
        'fingerprintTime': self.fingerprintTime,
        'testSamplesThrown': self.testSamplesThrown,
        'advSamplesThrown': self.advSamplesThrown,
        'testAcc': self.testAcc,
        'advAcc': self.advAcc,
        'testTime': self.testTime,
        'advTime': self.advTime,
        }

#being script
#holding variables
listInstances = []
currentThreshFingerprint = 0
currentFingerprintTime = 0
currentMagnif = 0

#open file and parse lines
with open(path + '/defense_gs.txt') as f:
    for line in f:
        if line.startswith('U'):
            #magnif settings line
            tokens = line.split(' ')
            currentMagnif = tokens[3]
        elif line.startswith('C'):
            #fingerprinting threshold and elapsed time line
            tokens = line.split(' ')
            currentThreshFingerprint = tokens[5]
            currentFingerprintTime = tokens[9]
        elif line.startswith('Starting'):
            #thresh definition line
            tokens = line.split(' ')
            threshDefense = tokens[7]
            
            #test samples line
            line = next(f)
            line = line.replace('/', ' ')
            tokens = line.split(' ')
            testSamplesThrown = tokens[3]
            
            #test samples acc and time
            line = next(f)
            tokens = line.split(' ')
            testAcc = tokens[6]
            testTime = tokens[8]
            
            #adv sample line
            line = next(f)
            line = line.replace('/', ' ')
            tokens = line.split(' ')
            advSamplesThrown = tokens[3]
            
            #adv sample acc and time
            line = next(f)
            tokens = line.split(' ')
            advAcc = tokens[6]
            advTime = tokens[8]
            
            #create instance and append to list
            listInstances.append(instance(currentMagnif, currentThreshFingerprint, threshDefense, 
                                          currentFingerprintTime, testSamplesThrown, advSamplesThrown,
                                          testAcc, advAcc, testTime, advTime))


#create dataframe and save to csv
df = pd.DataFrame.from_records([i.to_dict() for i in listInstances])
#rearrange columns
df = df[['magnif', 'fingerprintTime', 'threshFingerprint', 'threshDefense', 'testSamplesThrown', 'advSamplesThrown', 'testAcc', 'advAcc', 'testTime', 'advTime' ]]
df.to_csv(path + '/defense_gs.csv', index=False)
