'''
Sorts the neural network grid search log file for the CSS 590 Project.

@precondition: update path to directory of nngs_log.txt, file exists

@author: William Schneble
'''

import pandas as pd

#save files to log directory
path = "./logs/"

#filler
fill = ':-1'

#neural net object
class net():
    
    def __init__(self, layerOneNeurons, layerOneActFcn, layerTwoNeurons, 
                 layerTwoActFcn, layerThreeNeurons, layerThreeActFcn, trainFcn, accuracy, time):
        self.layerOneNeurons = cleanStr(layerOneNeurons)
        self.layerTwoNeurons = cleanStr(layerTwoNeurons)
        self.layerThreeNeurons = cleanStr(layerThreeNeurons)
        self.layerOneActFcn = cleanStr(layerOneActFcn)
        self.layerTwoActFcn = cleanStr(layerTwoActFcn)
        self.layerThreeActFcn = cleanStr(layerThreeActFcn)
        self.trainFcn = cleanStr(trainFcn)
        self.accuracy = cleanStr(accuracy)
        self.time = cleanStr(time)
        
    def __str__(self):
        str = 'L1N:' + self.layerOneNeurons + ' L1AF:' + self.layerOneActFcn
        
        if(self.layerTwoNeurons != '-1'):
            str += ' L2N:' + self.layerTwoNeurons + ' L2AF:' + self.layerTwoActFcn
            
        if(self.layerThreeNeurons != '-1'):
            str += ' L3N:' + self.layerThreeNeurons + ' L3AF:' + self.layerThreeActFcn
            
        str += ' Train:' + self.trainFcn + ' Acc:' + self.accuracy + ' Time:' + self.time
        return str

#for sorting by kernel type
def getKey(net):
    return net.accuracy

#get rid of the excess stuff
def cleanStr(str):
    tokens = str.split(":")
    return tokens[1]

#put the log data into net objects
listFrames = []
with open(path + 'nngs_log.txt') as f:
    for line in f:
        tokens = line.split(" ")
        
        if(len(tokens) == 10):
            myframe = net(tokens[0], tokens[1], tokens[2], tokens[3], tokens[4], tokens[5], tokens[6], tokens[7], tokens[8])
        elif(len(tokens) == 8):
            myframe = net(tokens[0], tokens[1], tokens[2], tokens[3], fill, fill, tokens[4], tokens[5], tokens[6])
        elif(len(tokens) == 6):
            myframe = net(tokens[0], tokens[1], fill, fill, fill, fill, tokens[2], tokens[3], tokens[4])
        else:
            continue
        
        listFrames.append(myframe)

#put the net objects into sorted log file with descending order
listFrames = sorted(listFrames, key=getKey, reverse=True)
with open(path + 'nngs_sorted_log.txt', 'w+') as f:
    for i in listFrames:
        f.write(i.__str__() + '\n')