%script extends nngs by changing the activation function on each layer

numActFcn = size(actFcn, 2);

%for number of layers, change each layer's transfer function
if layerThreeNeurons > 1
    for actLayerOneIdx = numActFcn:-1:2
        for actLayerTwoIdx = numActFcn:-1:2
            for actLayerThreeIdx = numActFcn:-1:2
                net = patternnet([layerOneNeurons, layerTwoNeurons, layerThreeNeurons]);
                net.trainFcn = char(trainFcn(trainIdx));
                net.layers{1}.transferFcn = char(actFcn(actLayerOneIdx));
                net.layers{2}.transferFcn = char(actFcn(actLayerTwoIdx));
                net.layers{3}.transferFcn = char(actFcn(actLayerThreeIdx));
                net.trainParam.showWindow = false;
                net.input.processFcns = {'mapminmax'}; tic;
                net = train(net, trainData, trainLabels, 'useGPU', 'yes'); elapsed_t = toc;
                pred = net(testData);
                perf = perform(net, testLabels, pred);
                fprintf(fp, 'L1N:%i L1AF:%s L2N:%i L2AF:%s L3N:%i L3AF:%s TrainFcn:%s Acc:%3.4f Time:%.4f \n', ...
                    layerOneNeurons, char(actFcn(actLayerOneIdx)), layerTwoNeurons, char(actFcn(actLayerTwoIdx)), ...
                    layerThreeNeurons, char(actFcn(actLayerThreeIdx)), char(trainFcn(trainIdx)), (1-perf)*100, elapsed_t);
            end
        end
    end
elseif layerTwoNeurons > 1
    for actLayerOneIdx = numActFcn:-1:2
        for actLayerTwoIdx = numActFcn:-1:2
            net = patternnet([layerOneNeurons, layerTwoNeurons]);
            net.trainFcn = char(trainFcn(trainIdx));
            net.layers{1}.transferFcn = char(actFcn(actLayerOneIdx));
            net.layers{2}.transferFcn = char(actFcn(actLayerTwoIdx));
            net.trainParam.showWindow = false;
            net.input.processFcns = {'mapminmax'}; tic;
            net = train(net, trainData, trainLabels, 'useGPU', 'yes'); elapsed_t = toc;
            pred = net(testData);
            perf = perform(net, testLabels, pred);
            fprintf(fp, 'L1N:%i L1AF:%s L2N:%i L2AF:%s TrainFcn:%s Acc:%3.4f Time:%.4f \n', ...
                layerOneNeurons, char(actFcn(actLayerOneIdx)), layerTwoNeurons, char(actFcn(actLayerTwoIdx)), ...
                char(trainFcn(trainIdx)), (1-perf)*100, elapsed_t);
        end
    end
else
    for actLayerOneIdx = numActFcn:-1:2
        net = patternnet([layerOneNeurons]);
        net.trainFcn = char(trainFcn(trainIdx));
        net.layers{1}.transferFcn = char(actFcn(actLayerOneIdx));
        net.trainParam.showWindow = false;
        net.input.processFcns = {'mapminmax'}; tic;
        net = train(net, trainData, trainLabels, 'useGPU', 'yes'); elapsed_t = toc;
        pred = net(testData);
        perf = perform(net, testLabels, pred);
        fprintf(fp, 'L1N:%i L1AF:%s TrainFcn:%s Acc:%3.4f Time:%.4f \n', ...
            layerOneNeurons, char(actFcn(actLayerOneIdx)), ...
            char(trainFcn(trainIdx)), (1-perf)*100, elapsed_t);
    end
end