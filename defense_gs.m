flog = fopen('./logs/defense_gs.txt', 'w+');

for magnif = 4:-0.3:2
    fprintf(flog, '--------------------------------------------\n');
    fprintf(flog, 'Using magnif = %f \n', magnif);
    fprintf(flog, '--------------------------------------------\n');
    for threshFingerprint = 51:-3:36
        run('fingerprint');
        for threshDefense = 51:-3:39
            run('defense');
        end
        fprintf(flog, '--------------------------------------------\n');
    end
end

fclose(flog);

system('python ./sortDefenseLog.py');