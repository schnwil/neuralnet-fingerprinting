%defense by training on the generated adversarial training samples

fprintf(flog, 'Training network with adversarial samples \n');

trainAdvData = cat(2, trainData, advDataTrain);
trainAdvLabels = cat(2, trainLabels, advLabelsTrain);

r = randperm(size(trainAdvData, 2));

trainAdvData = trainAdvData(:,r);
trainAdvLabels = trainAdvLabels(:,r);

netAdv = patternnet([100, 100, 100]);
netAdv.trainFcn = 'trainscg';
netAdv.layers{1}.transferFcn = 'tansig';
netAdv.layers{2}.transferFcn = 'tansig';
netAdv.layers{3}.transferFcn = 'radbas';
netAdv.trainParam.max_fail = 50;
netAdv.trainParam.showWindow = false;
netAdv.input.processFcns = {'mapminmax'}; tic;
netAdv = train(netAdv, trainAdvData, trainAdvLabels, 'useGPU', 'yes'); elapsed_t = toc;

%print results to command window
pred = netAdv(testData);
perf = perform(netAdv, testLabels, pred);
truths = vec2ind(testLabels) - 1;
class = vec2ind(pred) - 1;
testPred = class;
error = (nnz(truths - class)/size(testLabels, 2)*100);
testAcc = 100 - error;
fprintf(flog, 'Training completed in %.3f \n', elapsed_t);
fprintf(flog, 'MNIST Test Data:\n Perf:%3.2f Acc:%3.2f \n', (1-perf)*100, testAcc);

%test adversarial samples and print to window
pred = netAdv(advData);
perf = perform(netAdv, advLabels, pred);
truths = vec2ind(advLabels) - 1;
class = vec2ind(pred) - 1;
advPred = class;
error = (nnz(truths - class)/size(advLabels, 2)*100);
advAcc = 100 - error;
fprintf(flog, 'Adversarial Test:\n Perf:%3.2f Acc:%3.2f \n', (1-perf)*100, advAcc);

%cleanup
clear pred perf elapsed_t truths class error