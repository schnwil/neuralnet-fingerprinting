'''
Created on 5/26/17
Uses the defense_loop.txt log to create a CSV file

@author: William Schneble
'''
import pandas as pd

path = './logs'

class instance():
    def __init__(self, fraction, numNormalSamples, numAdvSamples, accNorm, accDefense, normalSamplesThrown, advSamplesThrown, elapsedTime):
        self.fraction = fraction
        self.numNormalSamples = numNormalSamples
        self.numAdvSamples = numAdvSamples
        self.accNorm = accNorm
        self.accDefense = accDefense
        self.normalSamplesThrown = normalSamplesThrown
        self.advSamplesThrown = advSamplesThrown
        self.elapsedTime = elapsedTime
        
    def __str__(self):
        return self.fraction + ' ' + self.numNormalSamples + ' ' + self.numAdvSamples + ' ' + self.accNorm + ' ' + self.accDefense + ' ' + self.normalSamplesThrown + ' ' + self.advSamplesThrown + ' ' + self.elapsedTime
    
    def to_dict(self):
        return {
        'fraction': self.fraction,
        'numNormalSamples': self.numNormalSamples,
        'numAdvSamples': self.numAdvSamples,
        'accNorm': self.accNorm,
        'accDefense': self.accDefense,
        'normalSamplesThrown': self.normalSamplesThrown,
        'advSamplesThrown': self.advSamplesThrown,
        'elapsedTime': self.elapsedTime
        }

#being script
listInstances = []

#open file and parse lines
with open(path + '/defense_loop.txt') as f:
    for line in f:
        if line.startswith('Using'):
            #fraction of mix and number of samples
            line = line.replace('(', '')
            tokens = line.split(' ')
            fraction = tokens[8]
            numNormalSamples = tokens[1]
            numAdvSamples = tokens[5]
        elif line.startswith('Accuracy'):
            #acc without defense line
            tokens = line.split(' ')
            accNorm = tokens[6]
        elif line.startswith('Fingerprinting'):
            #acc of defense and time
            tokens = line.split(' ')
            accDefense = tokens[3]
            elapsedTime = tokens[5]
        elif line.startswith('Normal'):
            #number of samples thrown line
            line = line.replace(',', '')
            tokens = line.split(' ')
            normalSamplesThrown = tokens[4]
            advSamplesThrown = tokens[10]
            
            #create instance and append to list
            listInstances.append(instance(fraction, numNormalSamples, numAdvSamples, accNorm, accDefense, normalSamplesThrown, advSamplesThrown, elapsedTime))


#create dataframe and save to csv
df = pd.DataFrame.from_records([i.to_dict() for i in listInstances])
#rearrange columns
df = df[['fraction', 'numNormalSamples', 'numAdvSamples', 'accNorm', 'accDefense', 'normalSamplesThrown', 'advSamplesThrown', 'elapsedTime' ]]
df.to_csv(path + '/defense_loop.csv', index=False)
