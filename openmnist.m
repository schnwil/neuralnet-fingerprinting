%script loads the mnist data

%load the training data
fp = fopen('.\data\train-images.idx3-ubyte', 'rb');
magic = fread(fp, 1, 'int32', 0, 'ieee-be');
numImages = fread(fp, 1, 'int32', 0, 'ieee-be');
numRows = fread(fp,1,'int32',0,'ieee-be');
numCols = fread(fp,1,'int32',0,'ieee-be');
images = fread(fp, inf,'unsigned char');
images = reshape(images, numCols, numRows, numImages);
trainImages = permute(images,[2 1 3]);
fp = fclose(fp);

%convert train data to vector format
trainData = reshape(trainImages, numCols*numRows, numImages);

%load the training labels
fp = fopen('.\data\train-labels.idx1-ubyte', 'rb');
magic = fread(fp, 1, 'int32', 0, 'ieee-be');
numLabels = fread(fp, 1, 'int32', 0, 'ieee-be');
trainLabels = fread(fp, inf, 'unsigned char');
fp = fclose(fp);

%load the test data
fp = fopen('.\data\t10k-images.idx3-ubyte', 'rb');
magic = fread(fp, 1, 'int32', 0, 'ieee-be');
numImages = fread(fp, 1, 'int32', 0, 'ieee-be');
numRows = fread(fp,1,'int32',0,'ieee-be');
numCols = fread(fp,1,'int32',0,'ieee-be');
images = fread(fp, inf,'unsigned char');
images = reshape(images, numCols, numRows, numImages);
testImages = permute(images,[2 1 3]);
fp = fclose(fp);

%convert train data to vector format
testData = reshape(testImages, numCols*numRows, numImages);

%load the test labels
fp = fopen('.\data\t10k-labels.idx1-ubyte', 'rb');
magic = fread(fp, 1, 'int32', 0, 'ieee-be');
numLabels = fread(fp, 1, 'int32', 0, 'ieee-be');
testLabels = fread(fp, inf, 'unsigned char');
fp = fclose(fp);

%one hot encode the labels
trainLabels = bsxfun(@eq, trainLabels(:), 0:9);
testLabels = bsxfun(@eq, testLabels(:), 0:9);

%convert test labels into double
trainLabels = double(trainLabels)';
testLabels = double(testLabels)';

%cleanup workspace
clear magic numCols numRows numLabels numImages images fp