from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import keras
import numpy as np
import os
import tensorflow as tf
from tensorflow.python.platform import app
from tensorflow.python.platform import flags

from cleverhans.attacks import SaliencyMapMethod
from cleverhans.utils import other_classes, cnn_model
from cleverhans.utils_mnist import data_mnist
from cleverhans.utils_tf import model_train, model_eval, model_argmax

FLAGS = flags.FLAGS

flags.DEFINE_string('filename', 'mnist.ckpt', 'Filename to save model under.')
flags.DEFINE_boolean('viz_enabled', True, 'Enable sample visualization.')
flags.DEFINE_integer('nb_epochs', 6, 'Number of epochs to train model')
flags.DEFINE_integer('batch_size', 128, 'Size of training batches')
flags.DEFINE_integer('nb_classes', 10, 'Number of classification classes')
flags.DEFINE_integer('img_rows', 28, 'Input row dimension')
flags.DEFINE_integer('img_cols', 28, 'Input column dimension')
flags.DEFINE_integer('nb_channels', 1, 'Nb of color channels in the input.')
flags.DEFINE_integer('nb_filters', 64, 'Number of convolutional filter to use')
flags.DEFINE_integer('nb_pool', 2, 'Size of pooling area for max pooling')
flags.DEFINE_integer('number_samples', 1000, 'Nb of adversarial samples to craft')
flags.DEFINE_boolean('test_data', True, 'Is this testing data or training data?')
flags.DEFINE_float('learning_rate', 0.1, 'Learning rate for training')


def main(argv=None):
    # Disable Keras learning phase since we will be serving through tensorflow
    keras.layers.core.K.set_learning_phase(0)

    # Set TF random seed to improve reproducibility
    tf.set_random_seed(1234)

    # Image dimensions ordering should follow the Theano convention
    if keras.backend.image_dim_ordering() != 'tf':
        keras.backend.set_image_dim_ordering('tf')
        print("INFO: '~/.keras/keras.json' sets 'image_dim_ordering' "
              "to 'th', temporarily setting to 'tf'")

    # Create TF session and set as Keras backend session
    sess = tf.Session()
    keras.backend.set_session(sess)
    print("Created TensorFlow session and set Keras backend.")

    # Get MNIST test data
    X_train, Y_train, X_test, Y_test = data_mnist(datadir='./data')
    print("Loaded MNIST test data.")

    # Define input TF placeholder
    x = tf.placeholder(tf.float32, shape=(None, 28, 28, 1))
    y = tf.placeholder(tf.float32, shape=(None, 10))

    # Define TF model graph
    model = cnn_model()
    preds = model(x)
    print("Defined TensorFlow model graph.")

    ###########################################################################
    # Training the model using TensorFlow
    ###########################################################################

    # Train an MNIST model if it does not exist in the train_dir folder
    saver = tf.train.Saver()
    save_path = './data/' + FLAGS.filename
    if os.path.isfile(save_path + '.meta'):
        saver.restore(sess, save_path)
    else:
        train_params = {
            'nb_epochs': FLAGS.nb_epochs,
            'batch_size': FLAGS.batch_size,
            'learning_rate': FLAGS.learning_rate
        }
        model_train(sess, x, y, preds, X_train, Y_train,
                    args=train_params)
        saver.save(sess, save_path)

    # Evaluate the accuracy of the MNIST model on legitimate test examples
    eval_params = {'batch_size': FLAGS.batch_size}
    accuracy = model_eval(sess, x, y, preds, X_test, Y_test,
                          args=eval_params)
    assert X_test.shape[0] == 10000, X_test.shape
    print('Test accuracy on legitimate test examples: {0}'.format(accuracy))

    ###########################################################################
    # Craft adversarial examples using the Jacobian-based saliency map approach
    ###########################################################################
    print('Crafting %i adversarial samples' % FLAGS.number_samples)
    
    #setup dictionary of known weaknesses from neural net confusion matrix
    adv_dict= {0:[5, 6, 8], 1:[2, 7], 2:[3, 7], 3:[2, 5, 8], 4:[7, 9], 5:[3, 6, 9], 6:[5, 8], 7:[9, 2], 8:[2, 3, 5, 9], 9:[3, 4, 7]}

    # Define the SaliencyMapMethod attack object
    jsma = SaliencyMapMethod(model, back='tf', sess=sess)
    
    #list of adversarial samples (pixel data), true labels and targeted labels
    adv_samples = []
    adv_true_labels = []
    adv_target_labels = []

    # Loop over the samples we want to perturb into adversarial examples
    sample_ind = 0 #current input index
    count = 0 #total number of adversarial samples crafted
    while count < FLAGS.number_samples:
        print('Attacking input %i with %i samples crafted so far...' % (sample_ind + 1, count))
        true_label = np.where(Y_test[sample_ind] == 1)[0]

        def generate_adv_sample(target):
            # This call runs the Jacobian-based saliency map approach
            one_hot_target = np.zeros((1, FLAGS.nb_classes), dtype=np.float32)
            one_hot_target[0, target] = 1
            jsma_params = {'theta': 1., 'gamma': 0.1,
                           'nb_classes': FLAGS.nb_classes, 'clip_min': 0.,
                           'clip_max': 1., 'targets': y,
                           'y_val': one_hot_target}
            adv_x = jsma.generate_np(X_test[sample_ind:(sample_ind+1)],
                                     **jsma_params)

            # Check if success was achieved
            pred = model_argmax(sess, x, preds, adv_x)
            print('True Label: %i Predicted Label: %i' % (np.where(Y_test[sample_ind] == 1)[0], pred))
            return adv_x
        
        #generate adversarial samples targeting weakness in neural net
        for target in adv_dict[int(true_label)]:
            adv_x = generate_adv_sample(target)
            adv_x = np.reshape(adv_x, (FLAGS.img_cols*FLAGS.img_rows, 1))
            adv_x = adv_x.T*255
            
            adv_samples.append(adv_x)
            adv_true_labels.append(true_label)
            adv_target_labels.append(target)
            count = count + 1
            if(count >= FLAGS.number_samples): break
        
        sample_ind = sample_ind + 1
        
    print('--------------------------------------')
    print('Saving %i samples to file...' % count, end='')
    
    #print samples and labels to file
    type = 'test'
    if FLAGS.test_data == False: 
        type = 'train'
    
    with open('./data/adv_samples_data_' + type + '.txt', 'wb') as f:
        for sample in adv_samples:
            np.savetxt(f, sample, fmt='%i')
    with open('./data/adv_samples_labels_' + type + '.txt', 'wb') as f:
        for label in adv_true_labels:
            np.savetxt(f, label, fmt='%i')
    with open('./data/adv_target_labels_' + type + '.txt', 'wb') as f:
        for target in adv_target_labels:
            tmpStr = '%i\n' % target
            f.write(tmpStr.encode())

    print(' Complete!')
    # Close TF session
    sess.close()
    

if __name__ == '__main__':
    app.run()
