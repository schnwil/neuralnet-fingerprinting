Fingerprinting with SIFT descriptors is a defensive measure to catch adversarial data on neural networks. The code provided is a series of scripts that can be executed to open the MNIST and adversarial data, generate the adversarial samples, create the fingerprinting database, and use the defense. A majority of the code is in MATLAB and requires the following modules: parallel computing, neural network toolbox, image processing toolbox, and vl_sift. Python (Anaconda3) dependencies include: Cleverhans, Numpy, Keras, Tensorflow and Pandas.

To run the MATLAB scripts you first need to run the Python file genAdvSamples.py which will download the MNIST dataset (if not already done so) and create the /data directory. To change the number of samples generated the flag 'number_samples' can be changed. The samples will be saved to three text files in the /data directory including a raw pixel data file, the target labels and the true (original) labels named adv_sample_data_test.txt, adv_target_labels_test.txt, and adv_samples_labels_test.txt respectively. To generate the adversarial training data change the flag test_data to False in genAdvSamples.py.

After the adversarial samples have been created, you can run the openmnist.m and openadv.m scripts in MATLAB. All scripts use the variable 'flog' to print to file. You can open flog with a specific file or use flog = 1 to print the results to the console window. All logs are saved in the /logs directory.

To train the neural network run the train_nn.m script. The resulting neural network will be saved with the variable name 'net'. To create the neural network with training on adversarial samples use the defense_train.m script, this network will be saved with the variable 'netAdv'.

To create the fingerprint database run the script fingerprint.m. If a fingerprint.mat file already exist in the /data directory, it will load this file rather than creating a new one.

To run the defense you must first run the fingerprint.m script.

All other scripts are parameter tuning and performance analysis scripts. The Python sorting scripts are called automatically from MATLAB and create their CSV or text files in the /logs directory.

The /materials directory contains confusion matrixes and spreed sheets not created by any of the scripts directly.