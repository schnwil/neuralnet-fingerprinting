%load the adversarial samples
pixelRow = 28;
pixelCol = 28;

%load pixel data for testing
[data] = textread('./data/adv_samples_data_test.txt', '%d');
numImages = size(data, 1)/(pixelRow*pixelCol);
data = reshape(data, pixelRow, pixelCol, numImages);
data = permute(data, [2 1 3]);
advImages = data;
advData = reshape(data, pixelRow*pixelCol, numImages);

%load adversarial true labels for testing
[advLabels] = textread('./data/adv_samples_labels_test.txt', '%d');
advLabels = bsxfun(@eq, advLabels(:), 0:9);
advLabels = advLabels';
advLabels = double(advLabels);

%load pixel data for training
[data] = textread('./data/adv_samples_data_train.txt', '%d');
numImages = size(data, 1)/(pixelRow*pixelCol);
data = reshape(data, pixelRow, pixelCol, numImages);
data = permute(data, [2 1 3]);
advImagesTrain = data;
advDataTrain = reshape(data, pixelRow*pixelCol, numImages);

%load adversarial true labels for training
[advLabelsTrain] = textread('./data/adv_samples_labels_train.txt', '%d');
advLabelsTrain = bsxfun(@eq, advLabelsTrain(:), 0:9);
advLabelsTrain = advLabelsTrain';
advLabelsTrain = double(advLabelsTrain);

%cleanup
clear data numImages pixelRow pixelCol