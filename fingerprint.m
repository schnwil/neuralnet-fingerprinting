%fingerprint the training data using vlFeat's SIFT. Create database of fingerprints per class label

%variables
binSize = 2;
stepSize = 2;
threshFingerprint = 51;
magnif = 3.7;
labels = vec2ind(trainLabels) - 1;

%if fingerprint db already made then load from file
if exist('./data/fingerprints.mat', 'file') == 2
    load('./data/fingerprints.mat');
    fprintf(flog, 'Loaded fingerprinting data from file \n');
    return;
end

%smooth the input images and convert to single
tmpImages = vl_imsmooth(trainImages, sqrt((binSize/magnif)^2 - 0.25));
tmpImages = single(tmpImages);
samples = size(trainData, 2)/6;

%fingerprint db
fingerprints = cell(10,2);

%add samples to fingerprint db
tic;
for sampleIdx = 1:1:samples
    [f,d] = vl_dsift(tmpImages(:,:,sampleIdx), 'size', binSize, 'step', stepSize);
    label = labels(sampleIdx) + 1;
    topMatch = 0;
    if size(fingerprints{label, 1}, 1) == 0
        fingerprints{label, 1} = d;
        fingerprints{label, 2} = f;
        fingerprints{label, 1} = cat(3, fingerprints{label, 1}, d);
        fingerprints{label, 2} = cat(3, fingerprints{label, 2}, f);
    else
        for fpIdx = 1:1:size(fingerprints{label, 1}, 3)
            matches = vl_ubcmatch(d,fingerprints{label,1}(:,:,fpIdx));
            if topMatch < size(matches, 2)
                topMatch = size(matches, 2);
            end
        end
        if topMatch < threshFingerprint
            fingerprints{label, 1} = cat(3, fingerprints{label, 1}, d);
            fingerprints{label, 2} = cat(3, fingerprints{label, 2}, f);
        end
    end
end

elapsed_t = toc;
fprintf(flog, 'Completed fingerprinting with tresh = %i and elapsed time: %.2f \n', threshFingerprint, elapsed_t);

%save fingerprint db to file
save('./data/fingerprints.mat', 'fingerprints');

%cleanup
clear f d fpIdx sampleIdx label labels matches samples tmpImages desSize threshFingerprint topMatch elapsed_t