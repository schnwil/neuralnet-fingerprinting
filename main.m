%run scripts

flog = fopen('./logs/main_log.txt', 'w+');

run('openmnist.m');
run('openadv.m');
run('train_nn.m');
run('fingerprint.m');
run('defense.m');
run('defense_train.m');
run('defense_test.m');

flog = fclose(flog);