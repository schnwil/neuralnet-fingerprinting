%script runs a grid search on pattern neural networks

%log of setups and scores
fp = fopen('.\logs\nngs_log.txt', 'w+');

%training functions
trainFcn = {'traincgp', 'trainscg'};

%activation functions
actFcn = {'none', 'radbas', 'tansig', 'elliotsig'};

%start gs
for trainIdx = size(trainFcn, 2):-1:1
    for layerOneNeurons = 100:-25:50
        for layerTwoNeurons = layerOneNeurons:-25:25
            for layerThreeNeurons = layerTwoNeurons:-25:25
                run('nngs_actfcn.m');
            end
            layerThreeNeurons = -1;
            run('nngs_actfcn.m');
        end
        layerTwoNeurons = -1;
        run('nngs_actfcn.m');
    end   
end

%close file and cleanup workspace
fp = fclose(fp);

system('python ./sortNetGridLog.py');

clear net fp perf pred actFcn actLayerOneIdx actLayerTwoIdx actLayerThreeIdx ...
    ans elapsed_t layerOneNeurons layerTwoNeurons layerThreeNeurons numActFcn trainFcn trainIdx

