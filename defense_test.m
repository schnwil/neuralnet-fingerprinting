%test the fingerprinting defense against mixed normal and adversarial
%samples

fprintf(flog, 'Starting evaluation of defense by mixing samples and testing against neural net \n');

%vars
totalSamples = 10000;
fractionAdv = 0.5;
threshDefense = 45;
numAdvSamples = int32(totalSamples*fractionAdv);
numNormalSamples = int32(totalSamples-numAdvSamples);

%mix adv and normal samples
mixedTest = cat(2, testData(:,1:numNormalSamples), advData(:,1:numAdvSamples));

%get true labels
truths = cat(2, testLabels(:,1:numNormalSamples), advLabels(:,1:numAdvSamples));
truths = vec2ind(truths) - 1;

%print num of samples used in mixing
fprintf(flog, 'Using %.0f normal samples and %.0f adversarial samples (%.2f adv:normal ratio) \n', numNormalSamples, numAdvSamples, fractionAdv);

%make predictions
pred = net(mixedTest);

%convert logical to number
class = vec2ind(pred) - 1;

%get number of misclassified samples
n = nnz(truths - class);

%print results of mixed on neural net
fprintf(flog, 'Accuracy of neural net without defense: %.2f \n', (1 - n/totalSamples) * 100);

%deploy defense

%convert to images
tmpImages = reshape(mixedTest, 28, 28, totalSamples);
tmpImages = vl_imsmooth(tmpImages, sqrt((binSize/magnif)^2 - 0.25));
tmpImages = single(tmpImages);

predIdx = zeros(1,size(truths,2));

count = 1; tic;
for sampleIdx = 1:1:totalSamples
    [f,d] = vl_dsift(tmpImages(:,:,sampleIdx), 'size', binSize, 'step', stepSize);
    for fpIdx = 1:1:size(fingerprints{truths(sampleIdx)+1, 1}, 3)
        m = vl_ubcmatch(d, fingerprints{truths(sampleIdx)+1, 1}(:,:,fpIdx));
       if threshDefense < size(m,2)
           break;
       elseif fpIdx == size(fingerprints{truths(sampleIdx)+1, 1}, 3)
           predIdx(1, count) = sampleIdx;
           count = count + 1;
       end
    end
end
predIdx = predIdx(1,1:count-1);

%get accuracy info
tmpData = mixedTest;
tmpData(:, predIdx) = [];
pred = net(tmpData);
pred = vec2ind(pred)-1;
tmpTestLabels = truths;
tmpTestLabels(:, predIdx) = [];
n = nnz(tmpTestLabels - pred);

%for every index below the number of normal samples, count as misclassify
advSamplesThrown = nnz(predIdx > numNormalSamples);
normalSamplesThrown = size(predIdx, 2) - advSamplesThrown;

%update number of non-zero, misclassified samples
n = n + normalSamplesThrown;

elapsed_t = toc;

fprintf(flog, 'Fingerprinting defense Acc: %.2f Time: %.3f \n', (1 - n/totalSamples) * 100, elapsed_t);
fprintf(flog, 'Normal samples thrown = %.0f, Adversarial samples thrown  = %.0f \n', normalSamplesThrown, advSamplesThrown);

%cleanup
clear n fpIdx ans normalSamplesThrown advSamplesThrown elapsed_t truth tmpTestLabels pred tmpData predIdx count tmpImages class numNormalSamples numAdvSamples mixedTest fractionAdv m f d
