%run the defense_test script with different fraction of adv:normal samples
%comment out the fractionAdv variable in defense_test.m before using

if flog > 1
   flog = fclose(flog); 
end

flog = fopen('./logs/defense_loop.txt', 'w+');

for fractionAdv = 1.0:-0.1:0
   fprintf(flog, '---------------------------------------\n');
   run('defense_test.m');
   fprintf(flog, '---------------------------------------\n');
end

flog = fclose(flog);

system('python ./sortDefenseLoopLog.py');

clear fractionAdv flogLoop