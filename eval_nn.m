%train neural net n times and report mean and standard deviation

itr = 10;
scoresTest = zeros(1, itr);
scoresAdv = zeros(1, itr);

if flog > 1
   flog = fclose(flog); 
end

flog = fopen('./logs/eval_log.txt', 'w+');

fprintf(flog, 'Running evaluation of neural net with training on normal samples and %d iterations \n', itr);

for n = 1:1:itr
   run('train_nn');
   scoresTest(n) = testAcc;
   scoresAdv(n) = advAcc;
end

meanTest = sum(scoresTest)/itr;
meanAdv = sum(scoresAdv)/itr;
devTest = std(scoresTest);
devAdv = std(scoresAdv);


fprintf(flog, 'Mean Accuracy Normal: %.2f +/- %.2f \n', meanTest, devTest);
fprintf(flog, 'Mean Accuracy Adversarial: %.2f +/- %.2f \n', meanAdv, devAdv);

fprintf(flog, '\nRunning evaluation of neural net with training on normal and adversarial samples and %d iterations \n', itr);

for n = 1:1:itr
   run('defense_train');
   scoresTest(n) = testAcc;
   scoresAdv(n) = advAcc;
end

meanTest = sum(scoresTest)/itr;
meanAdv = sum(scoresAdv)/itr;
devTest = std(scoresTest);
devAdv = std(scoresAdv);

fprintf(flog, 'Mean Accuracy Normal: %.2f +/- %.2f \n', meanTest, devTest);
fprintf(flog, 'Mean Accuracy Adversarial: %.2f +/- %.2f \n', meanAdv, devAdv);

clear itr scoresAdv scoresTest meanTest meanAdv devTest devAdv